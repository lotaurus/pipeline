package terminal

import "atomicgo.dev/cursor"

func GotoRelativePosition(relativePosition int) {
	cursor.Bottom()
	if relativePosition != 0 {
		cursor.Up(relativePosition)
	}
}

func Clear() {
	cursor.ClearLine()
}
