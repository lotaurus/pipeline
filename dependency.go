package pipeline

type Dependency interface {
	Result() bool
	IsDone() bool
}

type Independence struct{}

func (i *Independence) Result() bool { return true }
func (i *Independence) IsDone() bool { return true }
