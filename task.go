package pipeline

import (
	"fmt"
	"sync"
	"sync/atomic"

	"github.com/fatih/color"
	"github.com/slok/gospinner"
	"gitlab.com/lotaurus/pipeline/terminal"
)

type Work func(task *Task) bool

type Task struct {
	name             string
	message          string
	active           bool
	omitted          bool
	result           atomic.Bool
	done             atomic.Bool
	spinner          *gospinner.Spinner
	relativePosition atomic.Int64
	work             Work
	dependency       []Dependency
}

func NewTask(name string, work Work, dependency ...Dependency) *Task {
	newTask := &Task{
		name:       fmt.Sprintf("%s %s", "   |-", name),
		dependency: dependency,
		work:       work,
	}

	spinner, _ := gospinner.NewSpinnerWithColor(gospinner.Dots2, gospinner.FgHiGreen)
	spinner.SetMessage(newTask.name)

	fmt.Printf("%s %s\n\r", color.GreenString("\r⏸"), newTask.name)

	newTask.spinner = spinner
	newTask.message = newTask.name
	return newTask
}

func (t *Task) Message(msg string) {
	c := color.New(color.FgWhite).Add(color.BgHiBlack)
	t.message = fmt.Sprintf("%s :: %s", t.name, c.Sprintf(msg))
}

func (t *Task) Run(wg *sync.WaitGroup) {
	go func() {
		defer wg.Done()

		for {
			dependencyDone := true
			for _, dependency := range t.dependency {
				if !dependency.IsDone() {
					dependencyDone = false
				}
			}
			if !dependencyDone {
				continue
			}

			dependencySuccessfully := true
			for _, dependency := range t.dependency {
				if !dependency.Result() {
					dependencySuccessfully = false
				}
			}

			if !dependencySuccessfully {
				t.omitted = true
				t.done.Store(true)
				break
			}

			t.active = true
			t.result.Store(t.work(t))
			t.done.Store(true)
			t.active = false

			break
		}
	}()
}

func (t *Task) Render() {
	if t.active && !t.IsDone() {
		terminal.GotoRelativePosition(t.RelativePosition())
		t.spinner.SetMessage(t.message)
		_ = t.spinner.Render()
	}

	if !t.active && t.IsDone() {
		terminal.GotoRelativePosition(t.RelativePosition())
		terminal.Clear()
		if t.omitted {
			fmt.Printf("%s %s", color.HiGreenString("\r⛔"), t.name)
		} else {
			if t.Result() {
				fmt.Printf("%s %s", color.HiGreenString("\r✔"), t.message)
			} else {
				fmt.Printf("%s %s", color.HiRedString("\r✖"), t.message)
			}
		}
	}
}

func (t *Task) Result() bool {
	return t.result.Load()
}

func (t *Task) IsDone() bool {
	return t.done.Load()
}

func (t *Task) RelativePosition() int {
	return int(t.relativePosition.Load())
}

func (t *Task) SetRelativePosition(relativePosition int) {
	t.relativePosition.Store(int64(relativePosition))
}
