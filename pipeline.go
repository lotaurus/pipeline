package pipeline

import (
	"context"
	"fmt"
	"sync/atomic"
	"time"

	"atomicgo.dev/cursor"
	"github.com/fatih/color"
	"github.com/slok/gospinner"
	"gitlab.com/lotaurus/pipeline/terminal"
)

type Pipeline struct {
	name             string
	job              *Job
	done             atomic.Bool
	result           atomic.Bool
	relativePosition atomic.Int64
	spinner          *gospinner.Spinner
	lockMutex        atomic.Bool
	cancel           context.CancelFunc
}

func New(name string) *Pipeline {
	spinner, _ := gospinner.NewSpinnerWithColor(gospinner.Dots2, gospinner.FgHiGreen)
	spinner.SetMessage(name)
	_ = spinner.Render()
	fmt.Print("\n\r")
	terminal.GotoRelativePosition(0)
	pipeLine := &Pipeline{
		name:    name,
		job:     newJob(),
		spinner: spinner,
		cancel:  func() {},
	}
	pipeLine.relativePosition.Store(1)
	return pipeLine
}

func (p *Pipeline) StartAndWait() {
	if p.done.Load() {
		return
	}
	cursor.Hide()
	ctx, cancel := context.WithCancel(context.Background())
	p.cancel = cancel
	go p.render(ctx)
	p.job.RunAndWait()
	p.finish()
}

func (p *Pipeline) RelativePosition() int {
	return int(p.relativePosition.Load())
}

func (p *Pipeline) finish() {
	p.lock()
	defer p.unLock()
	p.cancel()
	p.done.Store(true)

	terminal.GotoRelativePosition(p.RelativePosition())
	p.result.Store(p.job.NoFailedResults())
	if p.Result() {
		fmt.Print(color.HiGreenString("\r✔ "))
	} else {
		fmt.Print(color.HiRedString("\r✖ "))
	}
	terminal.GotoRelativePosition(0)
	cursor.Show()
}

func (p *Pipeline) AddTask(name string, work Work, dependency ...Dependency) *Task {
	p.lock()
	defer p.unLock()

	terminal.GotoRelativePosition(0)
	newTask := NewTask(name, work, dependency...)
	p.job.Add(newTask)
	p.relativePosition.Add(1)
	p.job.RecalculateRelativePositions()
	return newTask
}

func (p *Pipeline) Result() bool {
	return p.result.Load()
}

func (p *Pipeline) render(ctx context.Context) {
	lastRender := time.Now()
	for {
		if ctx.Err() != nil {
			break
		}
		if time.Since(lastRender).Milliseconds() < 100 || p.isLocked() {
			continue
		}

		terminal.GotoRelativePosition(p.RelativePosition())
		_ = p.spinner.Render()

		p.job.Render()

		lastRender = time.Now()
	}
}

func (p *Pipeline) isLocked() bool {
	return p.lockMutex.Load()
}

func (p *Pipeline) lock() {
	p.lockMutex.Store(true)
}

func (p *Pipeline) unLock() {
	p.lockMutex.Store(false)
}
