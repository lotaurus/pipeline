module gitlab.com/lotaurus/pipeline

go 1.21

require (
	atomicgo.dev/cursor v0.2.0
	github.com/fatih/color v1.16.0
	github.com/slok/gospinner v0.1.1
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.15.0 // indirect
)
