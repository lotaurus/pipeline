package pipeline

import (
	"sync"
	"time"
)

type Job struct {
	tasks []*Task
}

func newJob() *Job {
	return &Job{tasks: make([]*Task, 0)}
}

func (j *Job) RunAndWait() {
	wg := &sync.WaitGroup{}
	wg.Add(j.CountTasks())

	for _, ts := range j.tasks {
		ts.Run(wg)
	}
	wg.Wait()
	time.Sleep(time.Second)
}

func (j *Job) RecalculateRelativePositions() {
	for i, ts := range j.tasks {
		ts.SetRelativePosition(len(j.tasks) - i)
	}
}

func (j *Job) Render() {
	for _, ts := range j.tasks {
		ts.Render()
	}
}

func (j *Job) NoFailedResults() bool {
	if len(j.tasks) == 0 {
		return true
	}

	for _, ts := range j.tasks {
		if !ts.Result() {
			return false
		}
	}
	return true
}

func (j *Job) NextPosition() int {
	var maxPosition = 0
	for i := range j.tasks {
		if maxPosition < j.tasks[i].RelativePosition() {
			maxPosition = j.tasks[i].RelativePosition()
		}
	}
	return maxPosition + 1
}

func (j *Job) Add(task *Task) int {
	j.tasks = append(j.tasks, task)
	return len(j.tasks) - 1
}

func (j *Job) CountTasks() int {
	return len(j.tasks)
}
